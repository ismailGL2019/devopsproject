package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsinkinsSonarTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsinkinsSonarTestProjectApplication.class, args);
	}

}
